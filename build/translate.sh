#!/bin/bash
set -e

while getopts ":d:f:p:l:" option; do
  case "${option}" in
    d)
        set -x
        ;;
    f)
        XCFFILE=${OPTARG}
        ;;
    p)
        PYFILE=${OPTARG}
        ;;
    l)
        I18N=${OPTARG}
        ;;
  esac
done

if [[ -z "$XCFFILE" || -z "$PYFILE" || -z "$I18N" ]]; then
    echo "usage: `basename $0` [-d] -p <pythonFuFile> -f <xcfFile> -l <language>"
    exit 1
fi

# use env to pass params
export XCFFILE
export I18N

# Start gimp with python-fu batch-interpreter
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    gimp -i --batch-interpreter=python-fu-eval -b "$(cat ${PYFILE})"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    /Applications/GIMP.app/Contents/MacOS/gimp -i --batch-interpreter=python-fu-eval -b "$(cat ${PYFILE})"
fi
